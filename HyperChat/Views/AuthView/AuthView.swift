//
//  ContentView.swift
//  HyperChat
//
//  Created by Akshat Sahu on 19/01/22.
//

import SwiftUI

struct AuthView: View {
    
    @State private var loginMode : Bool = false
    @State private var email : String = ""
    @State private var password : String = ""
    @State private var confirmPassword : String = ""
    @State private var name : String = ""

    
    var body: some View {
        NavigationView{
            VStack {
                Spacer()
                HStack{
                    Text("HyperChat")
                        .bold()
                        .font(.system(size: 30))
                    Spacer()
                }
                .padding(.vertical, 20)
                .padding(.horizontal, 20)
                .background(Color("light-grey"))
                
                Spacer(minLength: 40)
                
                Picker(selection: $loginMode, label:
                    Text("Auth mode Picker")
                ) {
                    Text("Login").tag(true)
                        .font(.system(size: 20))
                    Text("Register").tag(false)
                        .font(.system(size: 20))
                }.pickerStyle(SegmentedPickerStyle())
                    .frame(width: 400, height: 50, alignment: .center)
                
                VStack{
                    
                    Text(loginMode ? "Log in to your account" : "Create a new account")
                        .font(.system(size: 30))
                        .bold()
                    
                    if(!loginMode){
                        TextField("Full Name", text: $name)
                            .font(.system(size: 20))
                            .autocapitalization(.none)
                            .padding()
                            .background(Color("lightest-grey"))
                            .clipShape(RoundedRectangle(cornerRadius: 8))
                            .overlay(
                                    RoundedRectangle(cornerRadius: 8)
                                        .stroke(Color("light-grey"), lineWidth: 2)
                            )
                            .padding(.bottom, 10)
                    }
                    
                    TextField("Email", text: $email)
                        .font(.system(size: 20))
                        .autocapitalization(.none)
                        .padding()
                        .background(Color("lightest-grey"))
                        .clipShape(RoundedRectangle(cornerRadius: 8))
                        .overlay(
                                RoundedRectangle(cornerRadius: 8)
                                    .stroke(Color("light-grey"), lineWidth: 2)
                        )
                        .padding(.bottom, 10)
                        

                    
                    SecureField("Password", text: $password)
                        .font(.system(size: 20))
                        .padding()
                        .background(Color("lightest-grey"))
                        .clipShape(RoundedRectangle(cornerRadius: 8))
                        .overlay(
                                RoundedRectangle(cornerRadius: 8)
                                    .stroke(Color("light-grey"), lineWidth: 2)
                            )
                        .padding(.bottom, 10)

                    
                    if !loginMode {
                        SecureField("Confirm Password", text: $confirmPassword)
                            .font(.system(size: 20))
                            .padding()
                            .background(Color("lightest-grey"))
                            .clipShape(RoundedRectangle(cornerRadius: 8))
                            .overlay(
                                    RoundedRectangle(cornerRadius: 8)
                                        .stroke(Color("light-grey"), lineWidth: 2)
                                )
                            .padding(.bottom, 10)
                            
                        
                    }
                    
                    HStack{
                        Spacer()
                        Button {
                            if (loginMode) { handleLogin(email: email, password: password) }
                            else { handleRegister(email: email, password: password, confirmPassword: confirmPassword) }
                        } label: {
                            Text(loginMode ? "Login" : "Register")
                                .font(.system(size: 24))
                                .clipShape(RoundedRectangle(cornerRadius: 8))
                                .padding(.vertical, 12)
                                .padding(.horizontal, 35)

                        }
                        .background(Color("dark-grey"))
                        .clipShape(RoundedRectangle(cornerRadius: 8))
                        .foregroundColor(.black)
                    }
                    .padding(.top, 15)
                }
                .padding(.horizontal, 30)
                .padding(.vertical, 40)
                Spacer()
                Spacer()
                Spacer()
                Spacer()
                Spacer()
                

                
            }.ignoresSafeArea()
        }
        
        
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        AuthView()
    }
}
