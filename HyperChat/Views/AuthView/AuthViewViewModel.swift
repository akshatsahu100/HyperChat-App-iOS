//
//  AuthViewViewModel.swift
//  HyperChat
//
//  Created by Akshat Sahu on 20/01/22.
//

import Foundation

func handleLogin(email : String, password : String) {
    if(password == ""){
        print("Password field cannot be empty")
    }
    else if(email == ""){
        print("Please enter your email")
    }
    else {
        print("Logging In")
    }
   
}

func handleRegister(email : String, password : String, confirmPassword : String){
    if(email == ""){
        print("Please enter the email")
    }
    else if(confirmPassword != password){
        print("Passwords don't match")
    }
    else if(password == ""){
        print("Password field cannot be empty")
    }
    else{
        print("Registration call made")
    }
}
