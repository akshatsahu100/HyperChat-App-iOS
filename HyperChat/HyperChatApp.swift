//
//  HyperChatApp.swift
//  HyperChat
//
//  Created by Akshat Sahu on 19/01/22.
//

import SwiftUI

@main
struct HyperChatApp: App {
    var body: some Scene {
        WindowGroup {
            AuthView()
        }
    }
}
